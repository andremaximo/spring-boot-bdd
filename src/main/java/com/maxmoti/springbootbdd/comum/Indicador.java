package com.maxmoti.springbootbdd.comum;

import com.maxmoti.springbootbdd.pessoa.Pessoa;

import java.util.Arrays;

public enum Indicador {

    S("Sim"),N("Não");

    private String descricao;

    Indicador(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public static Indicador porDescricao(String descricao){
        return Arrays.asList(Indicador.values()).stream().filter(i -> i.getDescricao().equals(descricao)).findAny().get();
    }


}
