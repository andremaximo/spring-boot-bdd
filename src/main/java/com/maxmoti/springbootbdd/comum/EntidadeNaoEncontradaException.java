package com.maxmoti.springbootbdd.comum;

public class EntidadeNaoEncontradaException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public <E, K> EntidadeNaoEncontradaException(Class<E> classe, K idEntidade) {
        super("A entidade '" + classe.getSimpleName() + "' com o id '" + idEntidade.toString() + "' não foi encontrada.");
    }

    public <E> EntidadeNaoEncontradaException(Class<E> classe) {
        super("Nenhuma entidade '" + classe.getSimpleName() + "' que atendesse aos critérios desejados foi encontrada.");
    }

    public <E> EntidadeNaoEncontradaException(Class<E> classe, String criterios) {
        super("Nenhuma entidade '" + classe.getSimpleName() + "' que atendesse aos critérios informados foi encontrada. Critérios: " + criterios);
    }

}
