package com.maxmoti.springbootbdd.pessoa;

import com.maxmoti.springbootbdd.comum.Indicador;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

@Entity
@Table(name = "pessoa")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Pessoa {

    @ApiModelProperty(value = "Código da pessoa")
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty(value = "Nome da pessoa")
    @Column(name = "nome")
    @NotBlank(message = "Nome é um campo Obrigatório")
    private String nome;

    @ApiModelProperty(value = "Tipo da pessoa. F = Física, J = Jurídica")
    @Enumerated(value = EnumType.STRING)
    @Column(name = "tipoPessoa")
    @NotNull(message = "Tipo de Pessoa é um campo Obrigatório")
    private Tipo tipo;

    @ApiModelProperty(value = "Pessoa excluida. S = Sim, N = Não")
    @Enumerated(value = EnumType.STRING)
    @Column(name = "excluido")
    private Indicador excluido;

    @ApiModelProperty(value = "Pessoa ativa. S = Sim, N = Não")
    @Enumerated(value = EnumType.STRING)
    @Column(name = "ativo")
    private Indicador ativo;

    public enum Tipo {

        F("Física"),J("Jurídica");

        private String descricao;

        Tipo(String descricao){
            this.descricao = descricao;
        }

        public String getDescricao() {
            return descricao;
        }

        public static Tipo porDescricao(String descricao){
            return Arrays.asList(Tipo.values()).stream().filter(t -> t.getDescricao().equals(descricao)).findAny().get();
        }
    }
}
