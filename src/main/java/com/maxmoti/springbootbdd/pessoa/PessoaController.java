package com.maxmoti.springbootbdd.pessoa;

import com.maxmoti.springbootbdd.comum.EntidadeNaoEncontradaException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "pessoa", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PessoaController {

    private PessoaService pessoaService;
    private PessoaRepository pessoaRepository;

    @Autowired
    public PessoaController(PessoaService pessoaService, PessoaRepository pessoaRepository) {
        this.pessoaService = pessoaService;
        this.pessoaRepository = pessoaRepository;
    }

    @ApiOperation(value = "Salvar pessoa")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Pessoa salva com sucesso."),
            @ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Pessoa salvar(@Valid @RequestBody Pessoa pessoa){
        return pessoaService.salvar(pessoa);
    }

    @DeleteMapping("/{id}")
    public void excluir(@PathVariable Long id){
        pessoaService.excluir(id);
    }

    @GetMapping
    public List<Pessoa> get(Pessoa filtro){
        return pessoaRepository.findAll(PessoaRepository.ExampleFiltro.get(filtro));
    }

    @GetMapping("/{id}")
    public Pessoa get(@PathVariable Long id){
        return pessoaRepository.findById(id)
                .orElseThrow(() ->new EntidadeNaoEncontradaException(Pessoa.class, id));
    }
}
