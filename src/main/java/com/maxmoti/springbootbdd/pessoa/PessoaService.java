package com.maxmoti.springbootbdd.pessoa;

import com.maxmoti.springbootbdd.comum.EntidadeNaoEncontradaException;
import com.maxmoti.springbootbdd.comum.Indicador;
import com.maxmoti.springbootbdd.comum.NegocioException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;

@Service
public class PessoaService {

    private PessoaRepository pessoaRepository;

    @Autowired
    public PessoaService(PessoaRepository pessoaRepository) {
        this.pessoaRepository = pessoaRepository;
    }

    @Transactional
    public Pessoa salvar(Pessoa pessoa) {

        if(StringUtils.isEmpty(pessoa.getNome())){

            throw new NegocioException("Nome é um campo Obrigatório");

        }else if(pessoa.getTipo() == null){

            throw new NegocioException("Tipo de Pessoa é um campo Obrigatório");
        }

        pessoa.setAtivo(Indicador.S);
        pessoa.setExcluido(Indicador.N);

        return pessoaRepository.save(pessoa);
    }

    public void excluir(Long id) {

        Pessoa pessoa = pessoaRepository.findById(id)
                .orElseThrow(() -> new EntidadeNaoEncontradaException(Pessoa.class, id));

        pessoa.setExcluido(Indicador.S);
        pessoa.setAtivo(Indicador.N);

        pessoaRepository.save(pessoa);
    }
}
