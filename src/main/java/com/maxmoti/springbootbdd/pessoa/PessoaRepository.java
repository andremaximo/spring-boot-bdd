package com.maxmoti.springbootbdd.pessoa;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long>{

    static class ExampleFiltro{
        public static Example<Pessoa> get(Pessoa pessoa){
            return Example.of(pessoa, ExampleMatcher.matching()
                    .withIgnorePaths("id"));
        }
    }

}
