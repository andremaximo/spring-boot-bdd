package com.maxmoti.springbootbdd.veiculo;

import com.maxmoti.springbootbdd.comum.EntidadeNaoEncontradaException;
import com.maxmoti.springbootbdd.pessoa.Pessoa;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "veiculo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class VeiculoController {

    private VeiculoRepository veiculoRepository;
    private VeiculoService veiculoService;

    public VeiculoController(VeiculoRepository veiculoRepository, VeiculoService veiculoService) {
        this.veiculoRepository = veiculoRepository;
        this.veiculoService = veiculoService;
    }

    @PostMapping
    private Veiculo salvar(Veiculo veiculo){
        return veiculoService.salvar(veiculo);
    }

    @DeleteMapping("{id}")
    private void excluir(@PathVariable Long id){
        veiculoService.excluir(id);
    }

    @GetMapping("{id}")
    private Veiculo get(Long id){
        return veiculoRepository.findById(id)
                .orElseThrow(() -> new EntidadeNaoEncontradaException(Pessoa.class, id));
    }

    @GetMapping()
    private List<Veiculo> get(Veiculo filtro){
        return veiculoRepository.findAll();
    }
}
