package com.maxmoti.springbootbdd.veiculo;

import com.maxmoti.springbootbdd.comum.Indicador;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "veiculo")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Veiculo {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "placa")
    private String placa;
    @Enumerated(value = EnumType.STRING)
    @Column(name = "excluido")
    private Indicador excluido;
    @Enumerated(value = EnumType.STRING)
    @Column(name = "ativo")
    private Indicador ativo;
}
