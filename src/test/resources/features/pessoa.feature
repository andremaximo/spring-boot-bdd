Feature: Cadastro de Pessoa

  Scenario: Cadastrar uma pessoa
    Given não existem pessoas cadastras
    When o usuario cadastrar a pessoa
      | Nome  | Tipo Pessoa |
      | André | Física      |
    Then 1 pessoa é cadastrada
    And com os dados
      | Nome  | Tipo Pessoa | Excluído | Ativo |
      | André | Física      | Não      | Sim   |

  Scenario: Recuperar pessoa
    Given existem as seguintes pessoas cadastras
      | ID | Nome    | Tipo Pessoa | Excluido | Ativo |
      | 1  | André Z | Física      | Sim      | Não   |
      | 2  | Luis    | Física      | Não      | Sim   |
      | 3  | Felipe  | Física      | Sim      | Não   |
      | 4  | Capes   | Jurídica    | Não      | Sim   |
      | 5  | André M | Física      | Não      | Não   |
    When o usuario recuperar a pessoa 4
    Then a pessoa é recuperada
      | ID | Nome  | Tipo Pessoa | Excluido | Ativo |
      | 4  | Capes | Jurídica    | Não      | Sim   |

  Scenario: Recuperar pessoa excluida
    Given existem as seguintes pessoas cadastras
      | ID | Nome    | Tipo Pessoa | Excluido | Ativo |
      | 1  | André Z | Física      | Sim      | Não   |
      | 2  | Luis    | Física      | Não      | Sim   |
      | 3  | Felipe  | Física      | Sim      | Não   |
      | 4  | Capes   | Jurídica    | Não      | Sim   |
      | 5  | André M | Física      | Não      | Não   |
    When o usuario recuperar a pessoa 3
    Then o sistema nao retorna pois a pessoa esta excluida

  Scenario: Buscar pessoa por nome
    Given existem as seguintes pessoas cadastras
      | ID | Nome    | Tipo Pessoa | Excluido | Ativo |
      | 1  | André Z | Física      | Sim      | Não   |
      | 2  | Luis    | Física      | Não      | Sim   |
      | 3  | Felipe  | Física      | Sim      | Não   |
      | 4  | Capes   | Jurídica    | Não      | Sim   |
      | 5  | André M | Física      | Não      | Não   |
    When o usuario buscar por "Dre"
    Then os registros correspondentes são recuperados
      | ID | Nome    | Tipo Pessoa | Excluido | Ativo |
      | 1  | André Z | Física      | Sim      | Não   |
      | 5  | André M | Física      | Não      | Sim   |
