package com.maxmoti.springbootbdd.unitario.pessoa;

import com.maxmoti.springbootbdd.comum.Indicador;
import com.maxmoti.springbootbdd.comum.NegocioException;
import com.maxmoti.springbootbdd.pessoa.Pessoa;
import com.maxmoti.springbootbdd.pessoa.PessoaRepository;
import com.maxmoti.springbootbdd.pessoa.PessoaService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PessoaServiceTest {

    @Mock
    private PessoaRepository pessoaRepository;

    @InjectMocks
    private PessoaService pessoaService;

    private Pessoa pessoa;

    @Before
    public void setUp(){
        pessoa = Mockito.mock(Pessoa.class);
    }

    @Test
    public void salvarPessoaSucesso(){

        when(pessoa.getNome()).thenReturn("Andre");
        when(pessoa.getTipo()).thenReturn(Pessoa.Tipo.F);

        pessoaService.salvar(pessoa);

        verify(pessoa).setExcluido(Indicador.N);
        verify(pessoa).setAtivo(Indicador.S);
    }

    @Test(expected = NegocioException.class)
    public void salvarPessoaErroTipo(){

        when(pessoa.getNome()).thenReturn("Andre");
        pessoaService.salvar(pessoa);
    }

    @Test(expected = NegocioException.class)
    public void salvarPessoaErroNome(){
        pessoaService.salvar(pessoa);
    }

}
