package com.maxmoti.springbootbdd.integracao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maxmoti.springbootbdd.SpringBootBddApplication;
import com.maxmoti.springbootbdd.comum.Indicador;
import com.maxmoti.springbootbdd.pessoa.Pessoa;
import com.maxmoti.springbootbdd.pessoa.PessoaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootBddApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class PessoaTestIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PessoaRepository pessoaRepository;

    @Test
    public void inserirPessoa() throws Exception {

        Pessoa pessoa = Pessoa.builder()
                .nome("Luis")
                .tipo(Pessoa.Tipo.F)
                .build();

        String pessoaJson = mockMvc.perform(post("/pessoa")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(pessoa)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Pessoa pessoaInserida = objectMapper.readValue(pessoaJson, Pessoa.class);
        Pessoa pessoaInseridaRecuperada = pessoaRepository.findById(pessoaInserida.getId()).orElse(null);


        validarEstadoInicialPessoa(pessoaInserida);
        validarEstadoInicialPessoa(pessoaInseridaRecuperada);

        assertThat(pessoaInserida).isEqualToComparingFieldByField(pessoaInseridaRecuperada);
    }

    private void validarEstadoInicialPessoa(Pessoa pessoaInserida) {

        assertThat(pessoaInserida)
                .hasFieldOrPropertyWithValue("excluido", Indicador.N)
                .hasFieldOrPropertyWithValue("ativo", Indicador.S)
                .hasNoNullFieldsOrPropertiesExcept();
    }
}
