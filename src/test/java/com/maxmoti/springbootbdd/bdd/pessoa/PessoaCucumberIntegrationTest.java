package com.maxmoti.springbootbdd.bdd.pessoa;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/pessoa.feature", plugin = {"pretty", "html:target/cucumber"})
public class PessoaCucumberIntegrationTest {
}
