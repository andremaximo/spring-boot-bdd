package com.maxmoti.springbootbdd.bdd.pessoa;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maxmoti.springbootbdd.bdd.SpringBootBaseIntegrationTest;
import com.maxmoti.springbootbdd.infra.DBHelper;
import com.maxmoti.springbootbdd.pessoa.Pessoa;
import com.maxmoti.springbootbdd.pessoa.PessoaController;
import com.maxmoti.springbootbdd.pessoa.PessoaRepository;
import com.maxmoti.springbootbdd.pessoa.PessoaService;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Ignore
public class PessoaCucumberStepDefinitions extends SpringBootBaseIntegrationTest{

    public static final String NOME = "Nome";
    public static final String TIPO_PESSOA = "Tipo Pessoa";
    public static final String EXCLUIDO = "Excluído";
    public static final String ATIVO = "Ativo";

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private DBHelper dbHelper;

    @Given("^não existem pessoas cadastras$")
    public void não_existem_pessoas_cadastras() throws Exception {}

    @Before
    public void limparBanco(){
        dbHelper.clearDatabase();
    }

    @When("^o usuario cadastrar a pessoa$")
    public void o_usuario_cadastrar_a_pessoa(DataTable table) throws Exception {

        List<Map<String, String>> rows = table.asMaps(String.class, String.class);

        for (Map<String, String> row : rows) {

            Pessoa pessoa = Pessoa.builder()
                    .nome(row.get(NOME))
                    .tipo(Pessoa.Tipo.porDescricao(row.get(TIPO_PESSOA)))
                    .build();

            mockMvc.perform(post("/pessoa")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(pessoa)))
                    .andExpect(status().isOk());
        }
    }

    @Then("^(\\d+) pessoa é cadastrada$")
    public void uma_pessoa_é_cadastrada(int quatidade) throws Exception {
        Assert.assertEquals(quatidade, pessoaRepository.findAll().size());
    }

    @Then("^com os dados$")
    public void com_os_dados(DataTable table) throws Exception {

        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        List<Pessoa> pessoaList = pessoaRepository.findAll();

        for (Map<String, String> row : rows) {

            Pessoa pessoa = pessoaList.stream()
                    .filter(p -> p.getNome().equals(row.get(NOME)))
                    .findAny()
                    .get();

            Assert.assertEquals(row.get(NOME), pessoa.getNome());
            Assert.assertEquals(row.get(TIPO_PESSOA), pessoa.getTipo().getDescricao());
            Assert.assertEquals(row.get(EXCLUIDO), pessoa.getExcluido().getDescricao());
            Assert.assertEquals(row.get(ATIVO), pessoa.getAtivo().getDescricao());
        }
    }

    @Given("^existem as seguintes pessoas cadastras$")
    public void existem_as_seguintes_pessoas_cadastras(DataTable table) throws Exception {
        return;
    }

    @When("^o usuario recuperar a pessoa (\\d+)$")
    public void o_usuario_recuperar_a_pessoa(int arg1) throws Exception {

    }

    @Then("^a pessoa é recuperada$")
    public void a_pessoa_é_recuperada(DataTable arg1) throws Exception {
        
    }

    @Then("^o sistema nao retorna pois a pessoa esta excluida$")
    public void o_sistema_nao_retorna_pois_a_pessoa_esta_excluida() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        
    }

    @When("^o usuario buscar por \"([^\"]*)\"$")
    public void o_usuario_buscar_por_nDr(String valor) throws Exception {
        return;
    }

    @Then("^os registros correspondentes são recuperados$")
    public void os_registros_correspondentes_são_recuperados(DataTable arg1) throws Exception {

    }
}
