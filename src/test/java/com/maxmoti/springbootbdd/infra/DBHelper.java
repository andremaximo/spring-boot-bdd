package com.maxmoti.springbootbdd.infra;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.ext.h2.H2DataTypeFactory;
import org.hibernate.internal.SessionImpl;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

@Component
public class DBHelper {

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional
	public void clearDatabase() throws DBTestException {

		String shema = "PUBLIC";

		try{
			final SessionImpl ss = entityManager.unwrap(SessionImpl.class);
			final DatabaseConnection connection = new DatabaseConnection(ss.connection(), shema);
			connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new H2DataTypeFactory());

			Connection c = connection.getConnection();

			final Statement s = c.createStatement();

			// Disable FK
			s.execute("SET REFERENTIAL_INTEGRITY FALSE");

			// Find all tables and truncate them
			final Set<String> tables = new HashSet<>();
			ResultSet rs = s.executeQuery("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES  where TABLE_SCHEMA='" + shema+"'");
			while (rs.next()) {
				tables.add(rs.getString(1));
			}
			rs.close();
			for (final String table : tables) {
				s.executeUpdate("TRUNCATE TABLE "  + shema +  "." + table);
			}

			// Idem for sequences
			final Set<String> sequences = new HashSet<>();
			rs = s.executeQuery("SELECT SEQUENCE_NAME FROM INFORMATION_SCHEMA.SEQUENCES WHERE SEQUENCE_SCHEMA='" + shema + "'");
			while (rs.next()) {
				sequences.add(rs.getString(1));
			}
			rs.close();
			for (final String seq : sequences) {
				s.executeUpdate("ALTER SEQUENCE " + shema + "." + seq + " RESTART WITH 1");
			}

			// Enable FK
			s.execute("SET REFERENTIAL_INTEGRITY TRUE");
			s.close();
		}catch(DatabaseUnitException | SQLException e){
			throw new DBTestException("Erro ao limpar base de dados: " + shema, e);
		}
	}
}


