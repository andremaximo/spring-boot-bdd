package com.maxmoti.springbootbdd.infra;

public class DBTestException extends RuntimeException {

    public DBTestException(String message, Throwable cause) {
        super(message, cause);
    }
}
